
var path = "/home/esa_jaaskela/Surku/Surku.js"

var samplePath = "/home/esa_jaaskela/fate-suite/mp4"

var ld_library_path = "/home/esa_jaaskela/point-n-click/packages/builds/lib:/lib:/usr/lib:/usr/local/lib"

var crashStoragePath = "/home/esa_jaaskela/crash-storage"

//MARK Inputfile with %%INPUT%%
var executeTestApplicationCommand = ["ASAN_OPTIONS=coverage=1 /home/esa/sources/popplerasan/utils/pdftotext %%INPUT%%",
"ASAN_OPTIONS=coverage=1 /home/esa/sources/popplerasan/utils/pdftocairo %%INPUT%%",
"ASAN_OPTIONS=coverage=1 /home/esa/sources/popplerasan/utils/pdftohtml %%INPUT%%",
"ASAN_OPTIONS=coverage=1 /home/esa/sources/popplerasan/utils/pdftops %%INPUT%%"]

var fuzzedFileOutput = "/dev/shm/"

var spawnTestApplication = "/usr/local/bin/avconv"
var spawnTestApplicationFlags = ["-i","%INPUT%","temp.avi"]

var removableOutput = "/home/esa_jaaskela/msc-work/temp.avi"

var sancovPyPath = "/home/esa/llvm/projects/compiler-rt/lib/sanitizer_common/scripts/sancov.py"
var sancovOutputPrefix = ["rsvg-convert", "lt-pdftocairo","lt-pdftohtml","lt-pdftops"]

//Usually this is the working folder
var coverageFolders = ["/home/esa_jaaskela/msc-work"]

var fuzzedFileExtension = "mp4";

//Library to all the shared folders, used for calculating total coverage

var sharedLibraryFolders = ["/home/esa_jaaskela/point-n-click/packages/builds/lib"]


exports.getPath = function(){
	return path;
}

exports.getSamplePath = function(){
	return samplePath;
}

exports.getLd_library_path = function(){
	return ld_library_path;
}

exports.getCrashStoragePath = function(){
	return crashStoragePath;
}

exports.getExecuteTestApplicationCommand = function(){
	return executeTestApplicationCommand;
}

exports.getFuzzedFileOutput = function(){
	return fuzzedFileOutput;
}

exports.getSpawnTestApplicationCommand = function(){
	return spawnTestApplication;
}

exports.getSpawnTestApplicationCommandFlags = function(){
	return spawnTestApplicationFlags;
}

exports.getRemovableOutput = function(){
	return removableOutput;
}

exports.getSancovPyPath = function(){
	return sancovPyPath;
}

exports.getSancovOutputPrefix = function(){
	return sancovOutputPrefix;
}

exports.getCoverageFolders = function(){
	return coverageFolders;
}

exports.getFuzzedFileExtension = function(){
	return fuzzedFileExtension;
}

exports.getSharedLibraryFolders = function(){
	return sharedLibraryFolders;
}
