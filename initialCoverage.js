

//Get prefs object
var prefs = require("./prefs.js");

var fuzzedFileExtension = prefs.getFuzzedFileExtension();

var fuzzedFilePrefix = prefs.getSancovOutputPrefix()[0];

var fuzzedFileOutputFolder = prefs.getFuzzedFileOutput();

var spawnTestApplicationCommand = prefs.getSpawnTestApplicationCommand();

var originalFlags = prefs.getSpawnTestApplicationCommandFlags();

var samplePath = prefs.getSamplePath();

var removableOutput = prefs.getRemovableOutput();

var custom_ld_library_path = prefs.getLd_library_path();

var crashStoragePath = prefs.getCrashStoragePath();

//Get protobuf
var protobuf = require("./protobuf.js");

//Get Surku
var surkuPath = prefs.getPath();

//Get blessed
var blessed = require("blessed");
var contrib = require("blessed-contrib");

//Get filestream
var fs = require('fs');
var assert = require('assert');
var _ = require('lodash');

Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time
    if (this.length != array.length)
        return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;
        }
        else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
};

String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

// Create a screen object.
var screen = blessed.screen({
  smartCSR: true
});

screen.title = 'my window title';

// Create a box perfectly centered horizontally and vertically.
var box = blessed.box({
  top: 'top',
  left: 'left',
  width: '50%',
  height: '100%',
  content: 'Starting fuzzing!',
  tags: true,
  border: {
    type: 'line'
  },
  style: {
    fg: 'white',
    bg: 'black',
    border: {
      fg: '#f0f0f0'
    }
  }
});

var overviewbox = blessed.box({
  top: 'top',
  right:'',
  width: '50%',
  height: '50%',
  content: 'Total execs: \nAverage exec-time: \nTotal hangs: \nUnique hangs: \nTotal crashes: \nUnique crashes: \nTotal blocks: \nCovered blocks: ',
  tags: true,
  border: {
    type: 'line'
  },
  style: {
    fg: 'white',
    bg: 'black',
    border: {
      fg: '#f0f0f0'
    }
  }
});

var fuzzerBox = blessed.box({
  bottom:'',
  right:'',
  width: '50%',
  height: '50%',
  content: 'Total execs: \nAverage exec-time: \nTotal hangs: \nUnique hangs: \nTotal crashes: \nUnique crashes: \nTotal blocks: \nCovered blocks: ',
  tags: true,
  border: {
    type: 'line'
  },
  style: {
    fg: 'white',
    bg: 'black',
    border: {
      fg: '#f0f0f0'
    }
  }
});


var bar = contrib.bar(
       { label: 'Fuzzer scores',
         bottom:'',
        right:'',
        width: '50%',
        height: '50%',
        barWidth: 1,
        barSpacing: 1,
        xOffset: 0,
        maxHeight: 9,
        border: {
          type: 'line'
        },
        style: {
          fg: 'white',
          bg: 'black',
          border: {
            fg: '#f0f0f0'
          }
        }});

// Append our box to the screen.
screen.append(box);
screen.append(overviewbox);
screen.append(bar);



var lastRefresh = 0;

function logToConsole(msgToLog, forceRefresh){
  if(msgToLog == null)
    return;

  box.pushLine(""+msgToLog);

  //console.log(box.height);
  var boxContents = box.content.split("\n");
  while(boxContents.length > box.height-2){
    //console.log("SHIGTING");
    boxContents.shift();
  }
  var contentString = "";
  box.setContent("");
  for(var i = 0; i < boxContents.length; i++){
    box.pushLine(boxContents[i]);
  }
  var currentDate = new Date();
  if(currentDate - lastRefresh > 5000 || forceRefresh == 1){
    lastRefresh = currentDate;
    screen.render();
  }
}

box.key('enter', function(ch, key) {
  //console.log(box.height);

});

// Quit on Escape, q, or Control-C.
screen.key(['escape', 'q', 'C-c'], function(ch, key) {
  return process.exit(0);
});

// Render the screen.
screen.render();





//var sys = require('sys')
//Child process for executing the commands on shell
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;

//process.env['LD_LIBRARY_PATH'] = "";

var startTime = new Date();

logToConsole("Starting initial analysis", 0);

var initialAnalyse = "objdump -d "+prefs.getSpawnTestApplicationCommand()+" | grep '^\\s\\+[0-9a-f]\\+:.*\\scall\\(q\\|\\)\\s\\+[0-9a-f]\\+ <__sanitizer_cov\\(_with_check\\|\\)\\(@plt\\|\\)>' | wc -l";
var totalBlocks = 0;
exec(initialAnalyse,{env: {'LD_LIBRARY_PATH': ''}}, function (err, stdout, stderr){
  //console.log("objdump -d "+prefs.getSpawnTestApplicationCommand()+" | grep '^\s\+[0-9a-f]\+:.*\scall\(q\|\)\s\+[0-9a-f]\+ <__sanitizer_cov\(_with_check\|\)\(@plt\|\)>' | wc -l")
  //console.log("AAA "+stdout);

  totalBlocks = totalBlocks + parseInt(stdout.trim());
  //logToConsole(stdout.trim());
});

//process.env['LD_LIBRARY_PATH'] = custom_ld_library_path;


var initialAnalysisDone = 9999999;

var libraryAnalyse = "ldd "+prefs.getSpawnTestApplicationCommand();
exec(libraryAnalyse,{env: {'LD_LIBRARY_PATH': custom_ld_library_path}}, function(err,stdout,stderr){
  //console.log(stdout);
  //fs.appendFileSync("libraryanalyse.txt", "1"+ stdout + "\n");
  var lines = stdout.split("\n");
  //fs.appendFileSync("libraryanalyse.txt", lines.length);
  initialAnalysisDone = lines.length;
  for(var n = 0; n < lines.length; n++){
    try{
      var oneLine=lines[n].split("=>")[1].trim().split(" ")[0];

      //var env = Object.create( process.env );
      //env.LD_LIBRARY_PATH = custom_ld_library_path;

      process.env['LD_LIBRARY_PATH'] = "";

      var libraryInitialAnalyse = "objdump -d "+oneLine+" | grep '^\\s\\+[0-9a-f]\\+:.*\\scall\\(q\\|\\)\\s\\+[0-9a-f]\\+ <__sanitizer_cov\\(_with_check\\|\\)\\(@plt\\|\\)>' | wc -l";
      exec(libraryInitialAnalyse,{env: {'LD_LIBRARY_PATH': ''}}, function(err,stdout,stderr){
        initialAnalysisDone = initialAnalysisDone - 1;

        //fs.appendFileSync("librayanalyse.txt", stdout.trim()+"\n");
  if(!isNaN(parseInt(stdout.trim())))
        totalBlocks = totalBlocks + parseInt(stdout.trim());
      })
    }catch(err){
      initialAnalysisDone = initialAnalysisDone - 1;
    }
  }
});
console.log("initial analyisis");
process.env['LD_LIBRARY_PATH'] = custom_ld_library_path;

//Mutators of the surku and their initial scores
var score = {
  "freqString":10,
  "chunkSwapper":10,
  "chunkCollateTrigram":10,
  "regExpTrick":10,
  "strStuttr":10,
  "delimiterMutator":10,
  "mutateNumber":10,
  "replaceXMLValuePair":10,
  "strCopyShort":10,
  "strCopyLong":10,
  "strRemove":10,
  "insertSingleChar":10,
  "insertMultipleChar":10,
  "replaceSingleChar":10,
  "replaceMultipleChar":10,
  "repeatChar":10,
  "repeatChars":10,
  "bitFlip":10,
  "pdfObjectMangle":10,
  "xmlMutate":  10,
  };



var commandLineArguments = process.argv.slice(2);
//console.log(commandLineArguments);

var timeOut = -1;
if(commandLineArguments.indexOf('-t')!= -1){
  timeOut = new Date();
  timeOut = new Date(timeOut.getTime() + 1000*commandLineArguments[commandLineArguments.indexOf('-t')+1]);
  //timeOut = timeOut.addSeconds(1000*commandLineArguments[commandLineArguments.indexOf('-t')+1]);
  //console.log(timeOut);
}

var execLimit = -1;
if(commandLineArguments.indexOf('-a')!= -1){
  execLimit = parseInt(commandLineArguments[commandLineArguments.indexOf('-a')+1]);
}

var blackBox = 0;
if(commandLineArguments.indexOf('-bb')!=-1){
  blackBox = 1;
}

//Get surku fuzzer
var Surku=require(surkuPath);

//var newGenerator=new Surku({maxMutations:1,minMutations:1})
//newGenerator.mutators.changeMutatorWeight("freqString",1);
//newGenerator.mutators.updateMutators();

//Array of  the samples
var samples = [];

//Array of hang-objects
var hangs = [];
var totalHangAmount = 0;

//Array of crash-objects
var crashes = [];
var totalCrashAmount = 0;

//Get samples and add them to the sample array

var sampleFileNames = fs.readdirSync(samplePath);
for(var i = 0; i < sampleFileNames.length; i++){
  //sampleobject contains filename the times it has been analyzed
  var newSample = {};
  newSample.name = sampleFileNames[i];
  newSample['count'] = 0;
  samples.push(newSample);
}
//Array of testcommands got from prefs
var executeCommand = prefs.getExecuteTestApplicationCommand();

//individual mutators used in the test
var individualMutatorIndex = -1;
var individualArray = [];

var averageExecTime = -1;

//Object that contains the covered blocks and the amount of their appearance
var resultsArray = [];
for(var i = 0; i < executeCommand.length; i++){
  var resultsObject = {};
  resultsObject.name = executeCommand[i];
  resultsArray.push(resultsObject);
}
//console.log(resultsArray);



//Function to prepare the initial individuals for testing
function prepareIndividuals(){

  //Create 10 individuals
  for(var i = 0; i < 100; i++){
    //new individual object
    var individual = {};

    //Weight object containing the name of mutator and its weight
    var individualWeights = {};
    for(var key in score){
      //Randomize  the weights between 1 to 10
      if(blackBox == 0){
        individualWeights[key] = Math.floor((Math.random() * 10) + 1);
      }
      else{
        individualWeights[key] = 10;
      }
    }

    //TODO: set max and min mutations, possibly they could be constant in this test
    var newIndividual=new Surku({maxMutations:10,minMutations:1});
    //newIndividual.mutators.randomizeWeights();
    //newIndividual.mutators.updateMutators();

    //Set up individual object with weights, fuzzer object and initial score
    individual['weights'] = individualWeights;
    individual['fuzzer'] = newIndividual;
    individual['score'] = 10;

    //Push to individual array
    individualArray.push(individual);
    //console.log(individualArray);
    //console.log(createWeighedArray(individualArray));
  }
}

var totalResultObject = {};

var totalCoverageAmount = 0;

//Actual testing function. i is the index of test
function testCaseFunction(i){
  var startStamp = new Date();
  logToConsole("starting iteration "+i, 0);
  var currentResultObject = {};

  //Get filestream for reading a sample
  fs = require('fs');

  var titlesArray = [];
  var dataArray = [];
  for(var k = 0; k < individualArray.length; k++){
    titlesArray.push(""+k);
    dataArray.push(individualArray[k].score);
  }
  var barDataObject = {};
  barDataObject['titles'] = titlesArray;
  barDataObject['data'] = dataArray;
  bar.setData(barDataObject);
  
  var currentTime = new Date();
  var realAverageTime = (i/(currentTime - startTime))*1000;
  overviewbox.setContent('Total execs: '+i+'\nAverage exec-time: '+realAverageTime+'\nTotal hangs: '+totalHangAmount+'\nUnique hangs: '+hangs.length+'\nTotal crashes: '+totalCrashAmount+'\nUnique crashes: '+crashes.length+'\nTotal blocks: '+totalBlocks+'\nCovered blocks: '+totalCoverageAmount+'\nStart time:'+startTime+'\nCurrent time:'+currentTime);

  //Read random samplefile for testcase
  //TODO: maybe use some sort of scoring for samples too
  var oneSample = samples[i];
  var oneSampleName = oneSample['name'];
  var oneSamplePath = samplePath + "/" + oneSampleName;

  logToConsole("testing sample "+oneSamplePath, 1);

  fs.readFile(oneSamplePath, function (err,data){
    //If sample can't be read then return
    if (err) {
      if(err != null){
        logToConsole(err, 1);
        fs.appendFileSync("errorLog.txt","fs.readfile: "+err+"\n");
        testCaseFunction(i+1);
        return;
      }
    }

    var individualMutator = {};

    //Index of the individual that's used
    if(blackBox == 0){
      var weighedIndividualArray = createWeighedArray(individualArray);
      individualMutatorIndex = Math.floor(Math.random() * (weighedIndividualArray.length - 1 + 1) + 0);
      individualMutator = weighedIndividualArray[individualMutatorIndex];
    }
    else{
      individualMutator = individualArray[0];
    }
    //Get the individual object

    //Get the fuzzer object, and the weights for the fuzzer
    var fuzzer = individualMutator.fuzzer;
    var customWeights = individualMutator.weights;

    //Set up the fuzzer with custom mutator weights
    for(var key in customWeights){
      fuzzer.mutators.changeMutatorWeight(key, customWeights[key]);
    }

    //Update fuzzer
    fuzzer.mutators.updateMutators();

    //Create fuzzed testfile
    var testCase = individualMutator.fuzzer.generateTestCase(data);

    //Write the fuzzed file with the name fuzzed(testcasenumber).(extension)
    var fuzzedFileName = "fuzzed"+i+"-"+fuzzedFilePrefix+"."+fuzzedFileExtension;

    fs.writeFile(fuzzedFileOutputFolder+fuzzedFileName, testCase, function (err) {
      //If testcase cant be written then return
      if (err){
        if(err != null){
          logToConsole(err, 1);
          fs.appendFileSync("errorLog.txt","fs.writefile: "+err+"\n");
          testCaseFunction(i+1);
          return;
        }
      }
      //console.log("Past write");
      var children = {};
      var child;

      //var originalFlags = prefs.getSpawnTestApplicationCommandFlags();
      var arrayOfFlags = originalFlags.slice(0);

      var indexOfInput = arrayOfFlags.indexOf("%INPUT%");


      //logToConsole(oneSamplePath);
      //logToConsole(fuzzedFileOutputFolder + fuzzedFileName);
      if (indexOfInput !== -1) {
          arrayOfFlags[indexOfInput] = oneSamplePath;
          //arrayOfFlags[indexOfInput] = oneSamplePath;
      }

      var env = Object.create( process.env );
      env.ASAN_OPTIONS = 'coverage=1';
      env.LD_LIBRARY_PATH = custom_ld_library_path;

      child = spawn(spawnTestApplicationCommand, arrayOfFlags, {env:env});

      var childDead = 0;
      var childKilled = 0;
      var childCrashed = 0;
      var childOutput = "";
      var childError = "";

      var forceKillFunction = function forceKill(childToKill){
        if(childDead == 0){
          childToKill.kill();
          setTimeout(forceKill(childToKill),500);
        }
      }

      setTimeout(function(){
        if(childDead==0){
          logToConsole("Stopping child!",1);
          //logToConsole("Stopping child!",1);
          childKilled = 1;
          child.stdin.pause();
          child.kill('SIGSEGV');
          setTimeout(function(){
            if(childDead==0){
              child.kill();
              setTimeout(function(){
                if(childDead==0){
                  logToConsole("Commandline kill child!",1);
                  exec("kill "+child.pid,function (err, stdout, stderr){
                  });
                }  
              }, 1500);
            }
          }, 1500);
        }
      }, 1500);

      child.stdout.setEncoding('utf8');
      child.stdout.on('data',
          function (data) {
        //logToConsole(data);
              childOutput = childOutput.concat(" " + data);
          }
      );

      child.stderr.setEncoding('utf8');
      child.stderr.on('data',
          function (data) {
              //logToConsole(data);
              childError = childError.concat(" " + data);          
          }
      );

      child.on('exit', function (exitCode) {
        childDead  = 1;

        if(removableOutput.length > 0){
          try{
            fs.unlinkSync(removableOutput);
          }catch(e){
          } 
        }


        //logToConsole("ExitCode "+exitCode, 0);
        var arrayOfCoverageFolders = prefs.getCoverageFolders();

        var newCoverage = 0; 
        var newCrash = 0;
        var newHang = 0;


        for (var indexOfCoverageFolder = 0; indexOfCoverageFolder < arrayOfCoverageFolders.length; indexOfCoverageFolder = indexOfCoverageFolder + 1) {
          if(arrayOfCoverageFolders.hasOwnProperty(indexOfCoverageFolder)){
            var coverageFiles = fs.readdirSync(arrayOfCoverageFolders[indexOfCoverageFolder]);
            for(var oneFile in coverageFiles){
              if(coverageFiles.hasOwnProperty(oneFile)){
                if(String(coverageFiles[oneFile]).endsWith(".sancov")){
                  //Read the results of each *.sancov file to currentResultsObject
                  var library = String(coverageFiles[oneFile]).split(".")[0];

                  var libraryResultArray = readCoverageFile(coverageFiles[oneFile], arrayOfCoverageFolders[indexOfCoverageFolder], currentResultObject);

                  var arrayOfOldLines = totalResultObject[library];
                  if(typeof arrayOfOldLines === 'undefined'){
                    totalResultObject[library] = libraryResultArray;
                    totalCoverageAmount = totalCoverageAmount + libraryResultArray.length;
                  }
                  else{

                    var newLineArray = _.union(libraryResultArray,arrayOfOldLines);

                    if(newLineArray.length != arrayOfOldLines.length){
                      newCoverage = 1;
                      totalCoverageAmount = totalCoverageAmount + (newLineArray.length - arrayOfOldLines.length)
                    }

                    /*for(var m = 0; m < libraryResultArray.length; m++){
                      if(arrayOfOldLines.indexOf(libraryResultArray[m])==-1){
                        arrayOfOldLines.push(libraryResultArray[m]);
                        totalCoverageAmount = totalCoverageAmount + 1;
                        newCoverage = 1;
                      }
                    }*/
                    /*if(newCoverage == 1){
                      individualMutator.score = individualMutator.score + 10;
                      moveSample(fuzzedFileOutputFolder, fuzzedFileName, samplePath);                    
                    }*/
          

                    totalResultObject[library] = newLineArray;
                  }
                }
              }
            }
          }
        }

        //App was forcibly killed, check if this is new hang
        if(childKilled == 1){
          totalHangAmount = totalHangAmount + 1;
          //individualMutator.score = individualMutator.score + 10;

          //Check new hang if happened
          newHang = 1;
          for(var k = 0; k < hangs.length; k++){
            if(hangs.hasOwnProperty(k)){
              if(_.isEqual(hangs[k], currentResultObject)){
                newHang = 0;
                break;
              }
            }
          }
          if(newHang == 1){
            //individualMutator.score = individualMutator.score + 10;
            hangs.push(currentResultObject);
          }

        }

        //logToConsole(exitCode);
        
        //var re = /(==\d*==\s*ERROR:\s*AddressSanitizer);

        /*if(childOutput.match( /(==\d*==\s*ERROR:\s*AddressSanitizer)/ ) && childKilled==0){
            logToConsole(childOutput);
            fs.appendFileSync("asan_report.txt",fuzzedFileName + "\n" + childOutput);
        }
        if(childError.match( /(==\d*==\s*ERROR:\s*AddressSanitizer)/ && childKilled == 0)){
            logToConsole(childError);
            fs.appendFileSync("asan_report.txt",fuzzedFileName + "\n" + childError);
        }*/


        if((exitCode > 127 || exitCode == null || childOutput.match( /(==\d*==\s*ERROR:\s*AddressSanitizer)/ ) || childError.match( /(==\d*==\s*ERROR:\s*AddressSanitizer)/ )) && childKilled == 0 ){

          logToConsole("Something crashed!", 1);

          childCrashed = 1;

          totalCrashAmount = totalCrashAmount + 1;
          //individualMutator.score = individualMutator.score + 10;

          newCrash = 1;
          for(var k = 0; k < crashes.length; k++){
            if(crashes.hasOwnProperty(k)){
              if(_.isEqual(crashes[k],currentResultObject)){
                newCrash = 0;
                break;
              }
            }
          }

          //If newcrash (crash from new coverage)
          if(newCrash == 1){         
            //Check the last line of execution. if last line is same as in different coverage crash the crash is with high probability the same
            //HOwever the crash sample will be stored to different folder for later analysis
            currentResultObject['lastLineOfExecution'] = childError.split(/(==\d*==\s*ERROR:\s*AddressSanitizer)/)[2].split(/\n/)[2];
            if(typeof _.find(crashes, { 'lastLineOfExecution': childError.split(/(==\d*==\s*ERROR:\s*AddressSanitizer)/)[2].split(/\n/)[2] } ) === 'undefined' ){             
              fs.appendFileSync("newLine.txt",childError.split(/(==\d*==\s*ERROR:\s*AddressSanitizer)/)[2].split(/\n/)[2] + ": "+i+"\n");
              crashes.push(currentResultObject);
              //moveSample(fuzzedFileOutputFolder, fuzzedFileName, crashStoragePath, 0);
              
            }
            //else{
            //moveSample(fuzzedFileOutputFolder,fuzzedFileName, crashStoragePath, 0);
            //}
            //if(crashes.length == 0){
            //  crashes.push(currentResultObject);
            //}            
          }


          //Log asan output to a text file for later analysis
          if(childError.match( /(==\d*==\s*ERROR:\s*AddressSanitizer)/ ) || childOutput.match( /(==\d*==\s*ERROR:\s*AddressSanitizer)/ )){
            fs.appendFileSync("asan_report.txt","\n****************" + fuzzedFileName + "\n" + childError + "\n---------------" + childOutput + "\n****************");
          }

        }



        var results = resultsArray[0];


        //Every 40 iterations clean individual array down to 10 individuals
        if(i % 100 == 99){
          //array for individuals that will be preserved
          var newGenerationArray = [];

          individualArray.sort(compareByScore);

          var halveScores = 0;
          if(individualArray[individualArray.length-1].score > 10){
            halveScores = 1;
          }


          for(var k = 0; k < 40; k++){
            var newGenerationIndividual = individualArray[k];
            if(halveScores==1)
               newGenerationIndividual.score = Math.ceil(newGenerationIndividual.score/2);

            newGenerationArray.push(newGenerationIndividual);
          }

          //Create array containing all the individuals, weighed with their score
          var weighedArray = createWeighedArray(individualArray);

          //Get individuals until we have 10 acceptable individuals
          do{
            //Get random individual from weighed array
            var randomMutatorIndex = Math.floor(Math.random() * (weighedArray.length - 1 + 1) + 0);
            var randomNewGeneration = weighedArray[randomMutatorIndex];

            //check that array doesn't already have this individual
            if(newGenerationArray.indexOf(randomNewGeneration)==-1){
              if(halveScores==1)
                randomNewGeneration.score = Math.ceil(randomNewGeneration.score/2);
              newGenerationArray.push(randomNewGeneration);
            }
          }while(newGenerationArray.length <100)

          //put new individuals as individuals that will be used
          individualArray = newGenerationArray;
        }

        var endStamp = new Date();
        var difference = endStamp - startStamp;
        logToConsole("Iteration completed in "+difference+" millis", 0);              

        if(averageExecTime != -1){
          averageExecTime = (averageExecTime + difference)/2;
        } else {
          averageExecTime = difference;
        }

        if(i > sampleFileNames.length - 1){
          logToConsole("Fuzzing done! \nExit with CTRL + C", 1);
        }else testCaseFunction(i+1);
      });
    });
  });

}

//Crete individuals
prepareIndividuals();
//Start testing with index of 0


var timer = setTimeout(function() {
  if(initialAnalysisDone <=0){
    logToConsole("Analysis done, proceeding to fuzzing", 0);
    testCaseFunction(0);
  }
  else{
    timer = setTimeout(arguments.callee, 1000)
  }
}, 1000);


//testCaseFunction(5000);

//Function to create array with weighs
function createWeighedArray(listOfIndividuals){
  var weighedArray = [];
  //for each individual in array.
  for(var i = 0; i < listOfIndividuals.length; i++){
    var individualToWeigh = listOfIndividuals[i];
    //Add individual as many times as its score indicates
    for(var j = 0; j < individualToWeigh.score; j++){
      weighedArray.push(individualToWeigh);
    }

  }
  return weighedArray;
}


function readCoverageFile(filename, filepath, resultObject){

  var coverageDataFileName = filepath + "/" + filename;

  var coverageData = fs.readFileSync(coverageDataFileName);

  var len=coverageData.length;
  var indexOfCoverage=8;
  var resultCoverage=[];
  while(indexOfCoverage<len){
    //var offset=coverageData.readUInt32LE(indexOfCoverage);
    var offset = protobuf.readUInt64(coverageData,indexOfCoverage,'little');
    resultCoverage.push(offset);
    indexOfCoverage = indexOfCoverage + 8;
  }
  fs.unlink(coverageDataFileName, function(err){
    if(err != null){
      logToConsole(err, 1);
      fs.appendFileSync("errorLog.txt","fs.unlink (readCoverageFile): "+err+"\n");
      testCaseFunction(i+1);
      return;
    }
  });
  resultObject[filename.split(".")[0]] = resultCoverage;

  return resultCoverage;

}


process.on( 'SIGINT', function() {
  console.log( "\nGracefully shutting down from SIGINT (Ctrl-C)" );
  // some other closing procedures go here

  for(var j = 0; j < individualArray.length; j++){
    console.log("Fuzzer "+j+" score:" + individualArray[j].score);
  }

  process.exit( );
});
