//Get prefs object
var prefs = require('./prefs.js');

//Get Surku
var surkuPath = prefs.getPath();

//var sys = require('sys')
//Child process for executing the commands on shell
var exec = require('child_process').exec;

//var testCaseIndex = 0;

//Mutators of the surku and their initial scores
var score = {
	"freqString":10,
	"chunkSwapper":10,
	"chunkCollateTrigram":10,
	"regExpTrick":10,
	"strStuttr":10,
	"delimiterMutator":10,
	"mutateNumber":10,
	"replaceXMLValuePair":10,
	"strCopyShort":10,
	"strCopyLong":10,
	"strRemove":10,
	"insertSingleChar":10,
	"insertMultipleChar":10,
	"replaceSingleChar":10,
	"replaceMultipleChar":10,
	"repeatChar":10,
	"repeatChars":10,
	"bitFlip":10,
	"pdfObjectMangle":10,
	"xmlMutate":	10,
	};



//Get surku fuzzer
var Surku=require(surkuPath)

//var newGenerator=new Surku({maxMutations:1,minMutations:1})
//newGenerator.mutators.changeMutatorWeight("freqString",1);
//newGenerator.mutators.updateMutators();

//Array of  the samples
var samples = []

//Get samples and add them to the sample array
fs = require('fs');
sampleFileNames = fs.readdirSync(prefs.getSamplePath());
for(var i = 0; i < sampleFileNames.length; i++){
	//sampleobject contains filename the times it has been analyzed
	newSample = {};
	newSample['name'] = sampleFileNames[i];
	newSample['count'] = 0;
	samples.push(newSample);
}
//Array of testcommands got from prefs
var executeCommand = prefs.getExecuteTestApplicationCommand();

//individual mutators used in the test
var individualArray = []

//Object that contains the covered blocks and the amount of their appearance
var resultsArray = [];
for(var i = 0; i < executeCommand.length; i++){
	var resultsObject = {};
	resultsObject['name'] = executeCommand[i];
	resultsArray.push(resultsObject);
}
console.log(resultsArray);

//var results = {};

//Function to prepare the initial individuals for testing
function prepareIndividuals(){
	/*newGenerator.mutators.changeMutatorWeight("freqString",1);
	newGenerator.mutators.changeMutatorWeight("chunkSwapper",1);
	newGenerator.mutators.changeMutatorWeight("chunkCollateTrigram",1);
	newGenerator.mutators.changeMutatorWeight("regExpTrick",1);
	newGenerator.mutators.changeMutatorWeight("strStuttr",1);
	newGenerator.mutators.changeMutatorWeight("delimiterMutator",1);
	newGenerator.mutators.changeMutatorWeight("mutateNumber",1);
	newGenerator.mutators.changeMutatorWeight("replaceXMLValuePair",1);
	newGenerator.mutators.changeMutatorWeight("strCopyShort",1);
	newGenerator.mutators.changeMutatorWeight("strCopyLong",1);
	newGenerator.mutators.changeMutatorWeight("strRemove",1);
	newGenerator.mutators.changeMutatorWeight("insertSingleChar",1);
	newGenerator.mutators.changeMutatorWeight("insertMultipleChar",1);
	newGenerator.mutators.changeMutatorWeight("replaceSingleChar",1);
	newGenerator.mutators.changeMutatorWeight("replaceMultipleChar",1);
	newGenerator.mutators.changeMutatorWeight("repeatChar",1);
	newGenerator.mutators.changeMutatorWeight("repeatChars",1);
	newGenerator.mutators.changeMutatorWeight("bitFlip",1);
	newGenerator.mutators.changeMutatorWeight("pdfObjectMangle",1);
	newGenerator.mutators.changeMutatorWeight("xmlMutate", 0);
	newGenerator.mutators.updateMutators();*/
	//Create 10 individuals
	for(var i = 0; i < 10; i++){
		//new individual object
		var individual = {};

		//Weight object containing the name of mutator and its weight
		var individualWeights = {};
		for(var key in score){
			//Randomize  the weights between 1 to 10
			individualWeights[key] = Math.floor((Math.random() * 10) + 1);
		}

		//TODO: set max and min mutations, possibly they could be constant in this test
		var newIndividual=new Surku({maxMutations:10,minMutations:1});
		//newIndividual.mutators.randomizeWeights();
		//newIndividual.mutators.updateMutators();

		//Set up individual object with weights, fuzzer object and initial score
		individual['weights'] = individualWeights;
		individual['fuzzer'] = newIndividual;
		individual['score'] = 10;

		//Push to individual array
		individualArray.push(individual);

		//console.log(createWeighedArray(individualArray));
	}
}

//Actual testing function. i is the index of test
function testCaseFunction(i){
	console.log("starting iteration "+i);
	//Get filestream for reading a sample
	fs = require('fs');

	//Read random samplefile for testcase
	//TODO: maybe use some sort of scoring for samples too
	var oneSample = samples[Math.floor(Math.random()*samples.length)];
	var oneSampleName = oneSample['name'];
	var oneSamplePath = prefs.getSamplePath() + "/" + oneSampleName;
	fs.readFile(oneSamplePath, 'utf8', function (err,data) {

		//If sample can't be read then return
		if (err) {
			return console.log(err);
		}

		oneSample['count'] = oneSample['count'] + 1;

		//Index of the individual that's used
		weighedIndividualArray = createWeighedArray(individualArray);
		var individualMutatorIndex = Math.floor(Math.random() * (weighedIndividualArray.length - 1 + 1) + 0);
		//Get the individual object
		var individualMutator = weighedIndividualArray[individualMutatorIndex];

		//Get the fuzzer object, and the weights for the fuzzer
		var fuzzer = individualMutator.fuzzer;
		var customWeights = individualMutator.weights;

		//Set up the fuzzer with custom mutator weights
		for(var key in customWeights){
			fuzzer.mutators.changeMutatorWeight(key, customWeights[key]);
		}

		//Update fuzzer
		fuzzer.mutators.updateMutators();

		//Create fuzzed testfile
		var testCase = individualMutator.fuzzer.generateTestCase(data);

		//chose the application to test
		//Do this already here to get the index for fuzzed filename
		var executeCommandIndex = Math.floor(Math.random() * (executeCommand.length - 1 + 1) + 0);


		//Get the extension for the file (TODO: consider getting this from Prefs since most likely we always fuzz one type of file)
		var re =/(?:\.([^.]+))?$/;
		var extension = re.exec(oneSampleName)[1];

		//Write the fuzzed file with the name fuzzed(testcasenumber).(extension)
		var fuzzedFileName = "fuzzed"+i+"-"+prefs.getSancovOutputPrefix()[executeCommandIndex]+"."+extension;
		fs.writeFile(fuzzedFileName, testCase, function (err) {
			//If testcase cant be written then return
			if (err) return console.log(err);

			//Create the command actual command from the index
			var oneExecuteCommand = executeCommand[executeCommandIndex].replace("%%INPUT%%",fuzzedFileName);

			var children = {};
			var child;
			for(var processIndex = 0; processIndex<1;processIndex++){

				var spawnCommand = prefs.getSpawnTestApplicationCommand();

				var spawn = require('child_process').spawn;
				var arrayOfFlags = [];
				arrayOfFlags.push("/home/esa/msc-work/"+fuzzedFileName);


				var env = Object.create( process.env );
				env.ASAN_OPTIONS = 'coverage=1';
				//spawn( 'node', ['app.js'], { env: env } );


				child = spawn(spawnCommand, arrayOfFlags, {env:env});
				console.log(child.pid);

				child.stdout.on('data',
				    function (data) {
				        console.log('tail output: ' + data);
				    }
				);

				child.stderr.on('data',
				    function (data) {
				        console.log('err data: ' + data);
				    }
				);

				child.on('exit', function (exitCode) {
						console.log(prefs.getSancovOutputPrefix()[0]+"."+this.pid+".sancov");
						fs.unlinkSync(prefs.getSancovOutputPrefix()[0]+"."+this.pid+".sancov");
				    console.log("Child "+this.pid+" exited with code: " + exitCode);
				});

			}

			//Execute the command
			exec(oneExecuteCommand,  function (error, stdout, stderr) {

				//Create command to analyze the blocks covered
				var oneAnalyseCommand = prefs.getSancovPyPath() + " print " + prefs.getSancovOutputPrefix()[executeCommandIndex] + "*"
				exec(oneAnalyseCommand, function (error, stdout, stderr){

					//TODO: USE THIS FOR ANALYZING INSTEAD OF ABOVE PYTHON SCRIPT
					/*var coverageData = fs.readFileSync("lt-pdftotext.27823.sancov");
					var len=coverageData.length
					var indexOfCoverage=0;
					var resultCoverage=[]
					while(indexOfCoverage<len){
						var offset=coverageData.readUInt32LE(indexOfCoverage);
						resultCoverage.push(offset);
						indexOfCoverage = indexOfCoverage + 4;
					}
					console.log(resultCoverage);*/

					//Split the covered blocks into an array
					var lines = stdout.toString().split('\n');

					//variable to keep track if we found a new block
					var foundNewBlock = 0;

					var results = resultsArray[executeCommandIndex];

					//Go through each line and analyze if new blocks were covered
					lines.forEach(function(line) {
						if(line != ''){
							if(typeof results[line] === 'undefined'){
								//if new block found, mark foundNewBlock true
								results[line] = 1;
								foundNewBlock = 1;
							}
							else{
								//If old block, increase the amount of times block was covered
								results[line] = results[line]+1;
							}
						}
					});
					//If new block found add score to the individual and move fuzzed sample to sample directory
					if(foundNewBlock == 1){
						individualMutator.score = individualMutator.score + 10;
						console.log("\n"+"New block found:\n" + results['name']+ "\n");
						var moveInterestingSample = "cp "+fuzzedFileName+" "+prefs.getSamplePath()+"/"+fuzzedFileName;
						exec(moveInterestingSample, function(error,stdout,stderr){
							//TODO: Add sample to the sample array
							if(!err){
								newSample = {};
								newSample['name'] = fuzzedFileName;
								newSample['count'] = 0;
								samples.push(newSample);
							}
						});
					}
					//if nothing exciting happened remove the sample
					else{
						var removeUselessSample = "rm "+"fuzzed"+i+"*";
						exec(removeUselessSample, function(error,stdout,stderr){

						});
					}
					//Get random number for new generation probability
					var randomNewGenerationProbability = Math.random();

					//with five percent probability do  crossover
					if(randomNewGenerationProbability> 0.95){
						//Crossover

						//new crossover individuals
						var newIndividual1 = {};
						var newIndividual2 = {};

						//Get another individual to crossover with
						do{
							var anotherIndividualMutatorIndex = Math.floor(Math.random() * (individualArray.length - 1 + 1) + 0);
						}while(anotherIndividualMutatorIndex == individualMutatorIndex);
						var anotherIndividualMutator = individualArray[anotherIndividualMutatorIndex];

						//Make weight objects for new individuals
						var individualOneWeights = {};
						var individualTwoWeights = {};


						//var customWeights = individualMutator.weights;
						//iterate through every mutator and with 50-50 probability split them to new individuals
						for(var key in individualMutator.weights){
							var randomIndividualToCrossover = Math.random();
							if(randomIndividualToCrossover>0.5){
								individualOneWeights[key] = individualMutator.weights[key];
								individualTwoWeights[key] = anotherIndividualMutator.weights[key];
							}
							else{
								individualTwoWeights[key] = individualMutator.weights[key];
								individualOneWeights[key] = anotherIndividualMutator.weights[key];
							}



						}

						//create new fuzzer objects for the new individuals
						var firstNewIndividualFuzzer = new Surku({maxMutations:10,minMutations:1});
						var secondNewIndividualFuzzer = new Surku({maxMutations:10,minMutations:1});

						//Set up and push new individuals to the individual of arrays
						//TODO: Consider initial score. Maybe average of old individuals?
						newIndividual1['weights'] = individualOneWeights;
						newIndividual1['fuzzer'] = firstNewIndividualFuzzer;
						newIndividual1['score'] = 10;

						newIndividual2['weights'] = individualTwoWeights;
						newIndividual2['fuzzer'] = secondNewIndividualFuzzer;
						newIndividual2['score'] = 10;

						individualArray.push(newIndividual1);
						individualArray.push(newIndividual2);

					}
					//With five percent probability do mutation
					else if (randomNewGenerationProbability < 0.05){
						//Mutation
						//weights of mutators
						var customWeights = individualMutator.weights;

						//Prepare new mutated individual
						var mutatedIndividual = {}
						var mutatedIndividualWeights = {}

						//For each mutator decide if it will be mutated or not.
						for(var key in customWeights){
							var mutateOrNot = Math.random();
							//Mutate with 10% probability
							if(mutateOrNot < 0.10){
								//Mutate with amount of 1 to current weight
								var num = Math.floor(Math.random()*customWeights[key]) + 1;
								//Decide if we want to increase or decrease the weight mutator
								num *= Math.floor(Math.random()*2) == 1 ? 1 : -1;
								//Apply weight
								mutatedIndividualWeights[key] = customWeights[key] + num;
								//if weight is zero set it to 1
								if(mutatedIndividualWeights[key] < 1){
									mutatedIndividualWeights[key] = 1;
								}
							}
						}

						//Create new individual and add it to the array
						var mutatedIndividualFuzzer = new Surku({maxMutations:10,minMutations:1});

						mutatedIndividual['weights'] = mutatedIndividualWeights;
						mutatedIndividual['fuzzer'] = mutatedIndividualFuzzer;
						mutatedIndividual['score'] = individualMutator['score'];
						individualArray.push(mutatedIndividual);
					}



					//create command to remove the coverage info file generated
					//TODO: Maybe do this only 10 iterations for performans
					//var cleanCoverageInfo = "rm "+prefs.getSancovOutputPrefix()[executeCommandIndex] + "*" + " && rm libpoppler*"
					//exec(cleanCoverageInfo, function(error,stdout,stderr){
						//Log errors if any
						if (error !== null) {
							console.log('exec error: ' + error);
						}
						//Log coverage so far into a textfile
						logResults(executeCommandIndex, results);


						//Every 20 iterations clean individual array down to 10 individuals
						if(i % 20 == 0){
							//array for individuals that will be preserved
							newGenerationArray = []

							//Create array containing all the individuals, weighed with their score
							weighedArray = createWeighedArray(individualArray);

							//Get individuals until we have 10 acceptable individuals
							do{
								//Get random individual from weighed array
								randomMutatorIndex = Math.floor(Math.random() * (weighedArray.length - 1 + 1) + 0);
								randomNewGeneration = weighedArray[randomMutatorIndex];

								//check that array doesn't already have this individual
								if(newGenerationArray.indexOf(randomNewGeneration)==-1){
									newGenerationArray.push(randomNewGeneration);
								}
							}while(newGenerationArray.length <10)

							//put new individuals as individuals that will be used
							individualArray = newGenerationArray;
						}
						//if testcase is below 100 000, start new iteration recursively
						if(i < 100000){

								testCaseFunction(i+1);
						}
						//Else print out the final score of the fuzzers
						else{
							for(var j = 0; j < individualArray.length; j++){
									console.log("Fuzzer "+j+" score:" + individualArray[j].score);
							}
						}
					});
				//});
			});
		});
	});
}

//Crete individuals
prepareIndividuals();
//Start testing with index of 0
testCaseFunction(0);

//Function to create array with weighs
function createWeighedArray(listOfIndividuals){
	weighedArray = [];
	//for each individual in array.
	for(var i = 0; i < listOfIndividuals.length; i++){
		individualToWeigh = listOfIndividuals[i];
		//Add individual as many times as its score indicates
		for(var j = 0; j < individualToWeigh.score; j++){
			weighedArray.push(individualToWeigh);
		}

	}
	return weighedArray;
}

function logResults(executedCommandIndex, resultsObject){
	fs = require('fs');
	var count = -1;
	for(var key in resultsObject){
		count = count + 1;
	}

	fs.appendFile(executedCommandIndex+'.txt', count + "\n", function (err) {

	});
}
