//Get prefs object
var prefs = require('./prefs.js');

//Get protobuf
var protobuf = require('./protobuf.js');

//Get Surku
var surkuPath = prefs.getPath();

//Get blessed
var blessed = require('blessed');
var contrib = require('blessed-contrib');

Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time
    if (this.length != array.length)
        return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;
        }
        else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
}



// Create a screen object.
var screen = blessed.screen({
  smartCSR: true
});

screen.title = 'my window title';

// Create a box perfectly centered horizontally and vertically.
var box = blessed.box({
  top: 'top',
  left: 'left',
  width: '50%',
  height: '100%',
  content: 'Starting fuzzing!',
  tags: true,
  border: {
    type: 'line'
  },
  style: {
    fg: 'white',
    bg: 'black',
    border: {
      fg: '#f0f0f0'
    }
  }
});

var overviewbox = blessed.box({
  top: 'top',
  right:'',
  width: '50%',
  height: '50%',
  content: 'Total execs: \nAverage exec-time: \nTotal hangs: \nUnique hangs: \nTotal crashes: \nUnique crashes: \nTotal blocks: \nCovered blocks: ',
  tags: true,
  border: {
    type: 'line'
  },
  style: {
    fg: 'white',
    bg: 'black',
    border: {
      fg: '#f0f0f0'
    }
  }
});

var fuzzerBox = blessed.box({
  bottom:'',
  right:'',
  width: '50%',
  height: '50%',
  content: 'Total execs: \nAverage exec-time: \nTotal hangs: \nUnique hangs: \nTotal crashes: \nUnique crashes: \nTotal blocks: \nCovered blocks: ',
  tags: true,
  border: {
    type: 'line'
  },
  style: {
    fg: 'white',
    bg: 'black',
    border: {
      fg: '#f0f0f0'
    }
  }
});


var bar = contrib.bar(
       { label: 'Fuzzer scores',
         bottom:'',
        right:'',
        width: '50%',
        height: '50%',
        barWidth: 1,
        barSpacing: 1,
        xOffset: 0,
        maxHeight: 9,
        border: {
          type: 'line'
        },
        style: {
          fg: 'white',
          bg: 'black',
          border: {
            fg: '#f0f0f0'
          }
        }});




// Append our box to the screen.
screen.append(box);
screen.append(overviewbox);
screen.append(bar);



var lastRefresh = 0;

function logToConsole(msgToLog){
  //console.log(msgToLog);
  box.pushLine(""+msgToLog);

  //console.log(box.height);
  boxContents = box.content.split("\n");
  while(boxContents.length > box.height-2){
    //console.log("SHIGTING");
    boxContents.shift();
  }
  contentString = ""
  box.setContent("");
  for(var i = 0; i < boxContents.length; i++){
    box.pushLine(boxContents[i]);
  }
  var currentDate = new Date();
  if(currentDate - lastRefresh > 1000){
    lastRefresh = currentDate;
    screen.render();
  }
}

box.key('enter', function(ch, key) {
  //console.log(box.height);

});

// Quit on Escape, q, or Control-C.
screen.key(['escape', 'q', 'C-c'], function(ch, key) {
  return process.exit(0);
});

// Render the screen.
screen.render();





//var sys = require('sys')
//Child process for executing the commands on shell
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;

var initialAnalyse = "objdump -d "+prefs.getSpawnTestApplicationCommand()+" | grep '^\\s\\+[0-9a-f]\\+:.*\\scall\\(q\\|\\)\\s\\+[0-9a-f]\\+ <__sanitizer_cov\\(_with_check\\|\\)\\(@plt\\|\\)>' | wc -l"
var totalBlocks = -1;
exec(initialAnalyse, function (err, stdout, stderr){
  //console.log("objdump -d "+prefs.getSpawnTestApplicationCommand()+" | grep '^\s\+[0-9a-f]\+:.*\scall\(q\|\)\s\+[0-9a-f]\+ <__sanitizer_cov\(_with_check\|\)\(@plt\|\)>' | wc -l")
  //console.log("AAA "+stdout);
  totalBlocks = stdout.trim();
});


//var testCaseIndex = 0;

//Mutators of the surku and their initial scores
var score = {
	"freqString":10,
	"chunkSwapper":10,
	"chunkCollateTrigram":10,
	"regExpTrick":10,
	"strStuttr":10,
	"delimiterMutator":10,
	"mutateNumber":10,
	"replaceXMLValuePair":10,
	"strCopyShort":10,
	"strCopyLong":10,
	"strRemove":10,
	"insertSingleChar":10,
	"insertMultipleChar":10,
	"replaceSingleChar":10,
	"replaceMultipleChar":10,
	"repeatChar":10,
	"repeatChars":10,
	"bitFlip":10,
	"pdfObjectMangle":10,
	"xmlMutate":	10,
	};

var timeOut = -1;

var commandLineArguments = process.argv.slice(2);
console.log(commandLineArguments);

if(commandLineArguments.indexOf('-t')!= -1){
  timeOut = new Date();
  timeOut = new Date(timeOut.getTime() + 1000*commandLineArguments[commandLineArguments.indexOf('-t')+1]);
  //timeOut = timeOut.addSeconds(1000*commandLineArguments[commandLineArguments.indexOf('-t')+1]);
  console.log(timeOut);
}

//Get surku fuzzer
var Surku=require(surkuPath)

//var newGenerator=new Surku({maxMutations:1,minMutations:1})
//newGenerator.mutators.changeMutatorWeight("freqString",1);
//newGenerator.mutators.updateMutators();

//Array of  the samples
var samples = [];

//Array of hang-objects
var hangs = [];
var totalHangAmount = 0;

//Array of crash-objects
var crashes = [];
var totalCrashAmount = 0;

//Get samples and add them to the sample array
fs = require('fs');
sampleFileNames = fs.readdirSync(prefs.getSamplePath());
for(var i = 0; i < sampleFileNames.length; i++){
	//sampleobject contains filename the times it has been analyzed
	newSample = {};
	newSample['name'] = sampleFileNames[i];
	newSample['count'] = 0;
	samples.push(newSample);
}
//Array of testcommands got from prefs
var executeCommand = prefs.getExecuteTestApplicationCommand();

//individual mutators used in the test
var individualMutatorIndex = -1;
var individualArray = []

var averageExecTime = -1;

//Object that contains the covered blocks and the amount of their appearance
var resultsArray = [];
for(var i = 0; i < executeCommand.length; i++){
	var resultsObject = {};
	resultsObject['name'] = executeCommand[i];
	resultsArray.push(resultsObject);
}
//console.log(resultsArray);

//var results = {};

//Function to prepare the initial individuals for testing
function prepareIndividuals(){
	/*newGenerator.mutators.changeMutatorWeight("freqString",1);
	newGenerator.mutators.changeMutatorWeight("chunkSwapper",1);
	newGenerator.mutators.changeMutatorWeight("chunkCollateTrigram",1);
	newGenerator.mutators.changeMutatorWeight("regExpTrick",1);
	newGenerator.mutators.changeMutatorWeight("strStuttr",1);
	newGenerator.mutators.changeMutatorWeight("delimiterMutator",1);
	newGenerator.mutators.changeMutatorWeight("mutateNumber",1);
	newGenerator.mutators.changeMutatorWeight("replaceXMLValuePair",1);
	newGenerator.mutators.changeMutatorWeight("strCopyShort",1);
	newGenerator.mutators.changeMutatorWeight("strCopyLong",1);
	newGenerator.mutators.changeMutatorWeight("strRemove",1);
	newGenerator.mutators.changeMutatorWeight("insertSingleChar",1);
	newGenerator.mutators.changeMutatorWeight("insertMultipleChar",1);
	newGenerator.mutators.changeMutatorWeight("replaceSingleChar",1);
	newGenerator.mutators.changeMutatorWeight("replaceMultipleChar",1);
	newGenerator.mutators.changeMutatorWeight("repeatChar",1);
	newGenerator.mutators.changeMutatorWeight("repeatChars",1);
	newGenerator.mutators.changeMutatorWeight("bitFlip",1);
	newGenerator.mutators.changeMutatorWeight("pdfObjectMangle",1);
	newGenerator.mutators.changeMutatorWeight("xmlMutate", 0);
	newGenerator.mutators.updateMutators();*/
	//Create 10 individuals
	for(var i = 0; i < 10; i++){
		//new individual object
		var individual = {};

		//Weight object containing the name of mutator and its weight
		var individualWeights = {};
		for(var key in score){
			//Randomize  the weights between 1 to 10
			individualWeights[key] = Math.floor((Math.random() * 10) + 1);
		}

		//TODO: set max and min mutations, possibly they could be constant in this test
		var newIndividual=new Surku({maxMutations:10,minMutations:1});
		//newIndividual.mutators.randomizeWeights();
		//newIndividual.mutators.updateMutators();

		//Set up individual object with weights, fuzzer object and initial score
		individual['weights'] = individualWeights;
		individual['fuzzer'] = newIndividual;
		individual['score'] = 10;

		//Push to individual array
		individualArray.push(individual);
		//console.log(individualArray);
		//console.log(createWeighedArray(individualArray));
	}
}

//Actual testing function. i is the index of test
function testCaseFunction(i){
	var startStamp = new Date();
	logToConsole("starting iteration "+i);
	//Get filestream for reading a sample
	fs = require('fs');

  var titlesArray = [];
  var dataArray = [];
  for(var k = 0; k < individualArray.length; k++){
    titlesArray.push(""+k);
    dataArray.push(individualArray[k].score);
  }
  var barDataObject = {};
  barDataObject['titles'] = titlesArray;
  barDataObject['data'] = dataArray;
  bar.setData(barDataObject);

  overviewbox.setContent('Total execs: '+i+'\nAverage exec-time: '+averageExecTime+'\nTotal hangs: '+totalHangAmount+'\nUnique hangs: '+hangs.length+'\nTotal crashes: '+totalCrashAmount+'\nUnique crashes: '+crashes.length+'\nTotal blocks: '+totalBlocks+'\nCovered blocks: '+Object.keys(resultsArray[0]).length);

	//Read random samplefile for testcase
	//TODO: maybe use some sort of scoring for samples too
	var oneSample = samples[Math.floor(Math.random()*samples.length)];
	var oneSampleName = oneSample['name'];
	var oneSamplePath = prefs.getSamplePath() + "/" + oneSampleName;
  //console.log(oneSamplePath);
	fs.readFile(oneSamplePath, 'utf8', function (err,data) {
		//If sample can't be read then return
		if (err) {
      //console.log("Error reading");
			//console.log(err);
			return;
		}
    //console.log("AAA");
		//oneSample['count'] = oneSample['count'] + 1;

		//Index of the individual that's used
		weighedIndividualArray = createWeighedArray(individualArray);
		individualMutatorIndex = Math.floor(Math.random() * (weighedIndividualArray.length - 1 + 1) + 0);
		//Get the individual object
		var individualMutator = weighedIndividualArray[individualMutatorIndex];

		//var individualMutator = individualArray[individualArray.indexOf(tempindividualMutator)];


		//Get the fuzzer object, and the weights for the fuzzer
		var fuzzer = individualMutator.fuzzer;
		var customWeights = individualMutator.weights;

		//Set up the fuzzer with custom mutator weights
		for(var key in customWeights){
			fuzzer.mutators.changeMutatorWeight(key, customWeights[key]);
		}

		//Update fuzzer
		fuzzer.mutators.updateMutators();

		//Create fuzzed testfile
		var testCase = individualMutator.fuzzer.generateTestCase(data);

    var spawnTestApplicationCommand = prefs.getSpawnTestApplicationCommand();

		//Get the extension for the file (TODO: consider getting this from Prefs since most likely we always fuzz one type of file)
		var re =/(?:\.([^.]+))?$/;
		var extension = re.exec(oneSampleName)[1];

		//Write the fuzzed file with the name fuzzed(testcasenumber).(extension)
		var fuzzedFileName = "fuzzed"+i+"-"+prefs.getSancovOutputPrefix()[0]+"."+extension;
    //console.log(fuzzedFileName);
    //console.log(i);
    //console.log(prefs.getFuzzedFileOutput()+fuzzedFileName);
		fs.writeFile(prefs.getFuzzedFileOutput()+fuzzedFileName, testCase, function (err) {
			//If testcase cant be written then return
			if (err){
        //console.log("Error writing");
        //console.log(err);
        return ;
      }
      //console.log("Past write");
			var children = {};
			var child;

      var originalFlags = prefs.getSpawnTestApplicationCommandFlags();
      var arrayOfFlags = originalFlags.slice(0);

      var indexOfInput = arrayOfFlags.indexOf("%INPUT%");

      if (indexOfInput !== -1) {
        //console.log(i);
        //console.log(prefs.getFuzzedFileOutput);
        //console.log(prefs.getFuzzedFileOutput() + fuzzedFileName);
          arrayOfFlags[indexOfInput] = prefs.getFuzzedFileOutput() + fuzzedFileName;
      }
			//console.log(prefs.getFuzzedFileOutput() + fuzzedFileName);
      //console.log(fuzzedFileName);
      //console.log(arrayOfFlags);
      var env = Object.create( process.env );
      env.ASAN_OPTIONS = 'coverage=1';

      child = spawn(spawnTestApplicationCommand, arrayOfFlags, {env:env});

      //console.log("Spawned");
      var childDead = 0;
      var childKilled = 0;

      setTimeout(function(){
        if(childDead==0){
          childKilled = 1;
          //console.log('kill');
          child.stdin.pause();
          child.kill('SIGSEGV');
        }
      }, 1500);

      child.stdout.on('data',
          function (data) {
              //console.log('tail output: ' + data);
          }
      );

      child.stderr.on('data',
          function (data) {
              //console.log('err data: ' + data);
          }
      );

      child.on('exit', function (exitCode) {
        //console.log("Past execution");

        //App was forcibly killed, check if this is new hang
        if(childKilled == 1){
          totalHangAmount = totalHangAmount + 1;
          individualMutator.score = individualMutator.score + 10;
          //console.log("-------------------------------------------------------------------------------------");
          //console.log("Child killed");
          var coverageDataFileName = prefs.getSancovOutputPrefix()[0] + "."+ this.pid + ".sancov";
          var coverageData = fs.readFileSync(coverageDataFileName);
          //console.log("Got coverage!");

          var len=coverageData.length
          var indexOfCoverage=8;
          var resultCoverage=[]
          while(indexOfCoverage<len){
            //var offset=coverageData.readUInt32LE(indexOfCoverage);
            var offset = protobuf.readUInt64(coverageData,indexOfCoverage,'little');
            resultCoverage.push(offset);
            indexOfCoverage = indexOfCoverage + 8;
          }
          var newHang = 1;
          for(var k = 0; k < hangs.length; k++){
            if(hangs[k].equals(resultCoverage)){
              newHang = 0;
              break;
            }
          }
          if(newHang == 1){
            individualMutator.score = individualMutator.score + 10;
            hangs.push(resultCoverage);
          }
          //console.log(resultCoverage);
          //fs.unlinkSync(coverageDataFileName);



          //testCaseFunction(i+1);
          //return;
        }

        childDead  = 1;
				if(exitCode > 127){
          totalCrashAmount = totalCrashAmount + 1;
          individualMutator.score = individualMutator.score + 10;
          //console.log("-------------------------------------------------------------------------------------");
          //console.log("Error in sut");
          var coverageDataFileName = prefs.getSancovOutputPrefix()[0] + "."+ this.pid + ".sancov";
          var coverageData = fs.readFileSync(coverageDataFileName);
          //console.log("Got coverage!");

          var len=coverageData.length
          var indexOfCoverage=8;
          var resultCoverage=[]
          while(indexOfCoverage<len){
            //var offset=coverageData.readUInt32LE(indexOfCoverage);
            var offset = protobuf.readUInt64(coverageData,indexOfCoverage,'little');
            resultCoverage.push(offset);
            indexOfCoverage = indexOfCoverage + 8;
          }
          var newCrash = 1;
          for(var k = 0; k < crashes.length; k++){
            if(crashes[k].equals(resultCoverage)){
              newCrash = 0;
              break;
            }
          }
          if(newCrash == 1){
            individualMutator.score = individualMutator.score + 10;
            crashes.push(resultCoverage);
          }
          //console.log(resultCoverage);
          //fs.unlinkSync(coverageDataFileName);
          //console.log("ERROR IN SUT");
          //console.log(exitCode);
          //testCaseFunction(i+1);
					//return;
				}

        geneticAlgorithm(this.pid, individualMutator,fuzzedFileName);

        //console.log("onExit");
        //var childpid = this.pid;
        /*var coverageData = null;
        try{
          var coverageDataFileName = prefs.getSancovOutputPrefix()[0] + "."+ this.pid + ".sancov";
          coverageData = fs.readFileSync(coverageDataFileName);
        }
        catch(e){
          //console.log(prefs.getSancovOutputPrefix()[0] + "."+ this.pid + ".sancov");
          //console.log("Instrumentation not found!");
          logToConsole("Couldnt find coverage");
          testCaseFunction(i+1);
          return;

        }
        //console.log(coverageDataFileName);


        //console.log(coverageData);
        //console.log(coverageData[0]);
        var len=coverageData.length
        var indexOfCoverage=8;
        var resultCoverage=[]
        while(indexOfCoverage<len){
          //var offset=coverageData.readUInt32LE(indexOfCoverage);
          var offset = protobuf.readUInt64(coverageData,indexOfCoverage,'little');
          resultCoverage.push(offset);
          indexOfCoverage = indexOfCoverage + 8;
        }
        //console.log(resultCoverage);
        fs.unlinkSync(coverageDataFileName);

        //Split the covered blocks into an array
        //var lines = stdout.toString().split('\n');

        //variable to keep track if we found a new block
        var foundNewBlock = 0;

        var results = resultsArray[0];

        //Go through each line and analyze if new blocks were covered
        resultCoverage.forEach(function(line) {
          if(line != ''){
            if(typeof results[line] === 'undefined'){
              //if new block found, mark foundNewBlock true
              results[line] = 1;
              foundNewBlock = 1;
            }
            else{
              //If old block, increase the amount of times block was covered
              results[line] = results[line]+1;
            }
          }
        });
        //If new block found add score to the individual and move fuzzed sample to sample directory
        if(foundNewBlock == 1){
					//console.log(individualMutatorIndex);
					//console.log(individualMutator.score);
          individualMutator.score = individualMutator.score + 10;
					//console.log(individualMutator.score);
          //console.log("\n"+"New block found\n");
          try{
            var moveInterestingSample = "cp "+prefs.getFuzzedFileOutput()+fuzzedFileName+" "+prefs.getSamplePath()+"/"+fuzzedFileName;

  					var rd = fs.createReadStream(prefs.getFuzzedFileOutput()+fuzzedFileName);
  					var wr = fs.createWriteStream(prefs.getSamplePath()+"/"+fuzzedFileName);
  					wr.on("close", function(ex) {
  						//console.log("newSampleAdded");
  						newSample = {};
  						newSample['name'] = fuzzedFileName;
  						newSample['count'] = 0;
  						samples.push(newSample);

  						fs.unlink(prefs.getFuzzedFileOutput()+fuzzedFileName, function (err) {
  								//console.log(err);
  						});

  					});
  					rd.pipe(wr);
          }
          catch(e){
            //console.log("Error when copying");
            return;
          }


        }
        //if nothing exciting happened remove the sample
        else{
          //var removeUselessSample = "rm "+"fuzzed"+i+"*";
          try{
  					fs.unlink(prefs.getFuzzedFileOutput()+fuzzedFileName, function (err) {
  							//console.log(err);
  					});
          }
          catch(e){
            //console.log("Error when removing");
            return;
          }
          //exec(removeUselessSample, function(error,stdout,stderr){

          //});
        }
        //Get random number for new generation probability
        var randomNewGenerationProbability = Math.random();

        //with five percent probability do  crossover
        if(randomNewGenerationProbability> 0.95){
          //Crossover

          //new crossover individuals
          var newIndividual1 = {};
          var newIndividual2 = {};

          //Get another individual to crossover with
          do{
            var anotherIndividualMutatorIndex = Math.floor(Math.random() * (weighedIndividualArray.length - 1 + 1) + 0);
          }while(anotherIndividualMutatorIndex == individualMutatorIndex);
          var anotherIndividualMutator = weighedIndividualArray[anotherIndividualMutatorIndex];

          //Make weight objects for new individuals
          var individualOneWeights = {};
          var individualTwoWeights = {};


          //var customWeights = individualMutator.weights;
          //iterate through every mutator and with 50-50 probability split them to new individuals
          for(var key in individualMutator.weights){
            var randomIndividualToCrossover = Math.random();
            if(randomIndividualToCrossover>0.5){
              individualOneWeights[key] = individualMutator.weights[key];
              individualTwoWeights[key] = anotherIndividualMutator.weights[key];
            }
            else{
              individualTwoWeights[key] = individualMutator.weights[key];
              individualOneWeights[key] = anotherIndividualMutator.weights[key];
            }



          }

          //create new fuzzer objects for the new individuals
          var firstNewIndividualFuzzer = new Surku({maxMutations:10,minMutations:1});
          var secondNewIndividualFuzzer = new Surku({maxMutations:10,minMutations:1});

          //Set up and push new individuals to the individual of arrays
          //TODO: Consider initial score. Maybe average of old individuals?
          newIndividual1['weights'] = individualOneWeights;
          newIndividual1['fuzzer'] = firstNewIndividualFuzzer;
          newIndividual1['score'] = 10;

          newIndividual2['weights'] = individualTwoWeights;
          newIndividual2['fuzzer'] = secondNewIndividualFuzzer;
          newIndividual2['score'] = 10;

					//console.log(newIndividual1);
					//console.log(newIndividual2);

          individualArray.push(newIndividual1);
          individualArray.push(newIndividual2);

        }
        //With five percent probability do mutation
        else if (randomNewGenerationProbability < 0.05){
          //Mutation
          //weights of mutators
          var customWeights = individualMutator.weights;
					//console.log(individualMutator);
					//console.log("--------------------");
          //Prepare new mutated individual
          var mutatedIndividual = {}
          var mutatedIndividualWeights = {}

          //For each mutator decide if it will be mutated or not.
          for(var key in customWeights){
            var mutateOrNot = Math.random();
            //Mutate with 10% probability
            if(mutateOrNot < 0.10){
              //Mutate with amount of 1 to current weight
              var num = Math.floor(Math.random()*customWeights[key]) + 1;
              //Decide if we want to increase or decrease the weight mutator
              num *= Math.floor(Math.random()*2) == 1 ? 1 : -1;
              //Apply weight
              mutatedIndividualWeights[key] = customWeights[key] + num;
              //if weight is zero set it to 1
              if(mutatedIndividualWeights[key] < 1){
                mutatedIndividualWeights[key] = 1;
              }
            }
						else{
							//Apply weight
							mutatedIndividualWeights[key] = customWeights[key];
						}
          }

          //Create new individual and add it to the array
          var mutatedIndividualFuzzer = new Surku({maxMutations:10,minMutations:1});

          mutatedIndividual['weights'] = mutatedIndividualWeights;
          mutatedIndividual['fuzzer'] = mutatedIndividualFuzzer;
					//TODO: Consider if this is 10 or prev score
          mutatedIndividual['score'] = 10;
					//console.log(individualMutator);
					//console.log("--------------------");
					//console.log(mutatedIndividual);
          individualArray.push(mutatedIndividual);
        }

        //Log errors if any
        //if (error !== null) {
          //console.log('exec error: ' + error);
        //}
        //Log coverage so far into a textfile
				if(i % 5 == 0){
        	logResults(results, individualArray,i);
				}

        //Every 40 iterations clean individual array down to 10 individuals
        if(i % 40 == 39){
          //array for individuals that will be preserved
          newGenerationArray = []

          individualArray.sort(compareByScore);

          for(var k = 0; k < 4; k++){
            newGenerationArray.push(individualArray[k]);
          }

          //Create array containing all the individuals, weighed with their score
          weighedArray = createWeighedArray(individualArray);

          //Get individuals until we have 10 acceptable individuals
          do{
            //Get random individual from weighed array
            randomMutatorIndex = Math.floor(Math.random() * (weighedArray.length - 1 + 1) + 0);
            randomNewGeneration = weighedArray[randomMutatorIndex];

            //check that array doesn't already have this individual
            if(newGenerationArray.indexOf(randomNewGeneration)==-1){
              newGenerationArray.push(randomNewGeneration);
            }
          }while(newGenerationArray.length <10)

          //put new individuals as individuals that will be used
          individualArray = newGenerationArray;
        }

				var endStamp = new Date();
				var difference = endStamp - startStamp;
				logToConsole("Iteration completed in "+difference+" millis");

        if(averageExecTime != -1){
          averageExecTime = (averageExecTime + difference)/2;
        } else {
          averageExecTime = difference;
        }*/

				//console.log(individualArray.length);
        //if testcase is below 100 000, start new iteration recursively
        //if(i < 10000){

            //testCaseFunction(i+1);

        var results = resultsArray[0];

        //Log coverage so far into a textfile
        if(i % 5 == 0){
          logResults(results, individualArray,i);
        }

        //Every 40 iterations clean individual array down to 10 individuals
        if(i % 40 == 39){
          //array for individuals that will be preserved
          newGenerationArray = []

          individualArray.sort(compareByScore);

          for(var k = 0; k < 4; k++){
            newGenerationArray.push(individualArray[k]);
          }

          //Create array containing all the individuals, weighed with their score
          weighedArray = createWeighedArray(individualArray);

          //Get individuals until we have 10 acceptable individuals
          do{
            //Get random individual from weighed array
            randomMutatorIndex = Math.floor(Math.random() * (weighedArray.length - 1 + 1) + 0);
            randomNewGeneration = weighedArray[randomMutatorIndex];

            //check that array doesn't already have this individual
            if(newGenerationArray.indexOf(randomNewGeneration)==-1){
              newGenerationArray.push(randomNewGeneration);
            }
          }while(newGenerationArray.length <10)

          //put new individuals as individuals that will be used
          individualArray = newGenerationArray;
        }





        var endStamp = new Date();
        var difference = endStamp - startStamp;
        logToConsole("Iteration completed in "+difference+" millis");

        if(averageExecTime != -1){
          averageExecTime = (averageExecTime + difference)/2;
        } else {
          averageExecTime = difference;
        }

        if(timeOut != - 1){
          if(new Date() < timeOut){
            testCaseFunction(i+1);
          }
          else{
            logToConsole("Fuzzing done!\nExit with CTRL + C");
          }
        }
        

        //}
        //Else print out the final score of the fuzzers
        //else{

        //}
		  });
	  });
	});
}

//Crete individuals
prepareIndividuals();
//Start testing with index of 0
testCaseFunction(0);
//testCaseFunction(5000);

//Function to create array with weighs
function createWeighedArray(listOfIndividuals){
	weighedArray = [];
	//for each individual in array.
	for(var i = 0; i < listOfIndividuals.length; i++){
		individualToWeigh = listOfIndividuals[i];
		//Add individual as many times as its score indicates
		for(var j = 0; j < individualToWeigh.score; j++){
			weighedArray.push(individualToWeigh);
		}

	}
	return weighedArray;
}

function logResults(resultsObject,individualArray, iteration){

	fs = require('fs');
	var count = -1;
	for(var key in resultsObject){
		count = count + 1;
	}

	fs.appendFile('/dev/shm/coveredBlocks.txt', iteration +","+ count + "\n", function (err) {

	});
	var cumulativeWeightsOfMutators = {};
	var totalSumOfWeights = 0;


	for(var individual in individualArray){
		var individualObjectWeights = individualArray[individual].weights;
		for(var weight in individualObjectWeights){
			//console.log(individualObjectWeights[weight]);
			totalSumOfWeights = totalSumOfWeights + individualObjectWeights[weight];
			//console.log(individualObjectWeights[weight]);
			if(typeof cumulativeWeightsOfMutators[weight]==='undefined'){
				cumulativeWeightsOfMutators[weight] = individualObjectWeights[weight]
			}else{
				cumulativeWeightsOfMutators[weight] = cumulativeWeightsOfMutators[weight] + individualObjectWeights[weight]
			}

		}
	}
//console.log(totalSumOfWeights);
if(totalSumOfWeights != totalSumOfWeights){
	process.exit(1);
}
	var relativeWeightsOfMutators = {};
	for(var weight in cumulativeWeightsOfMutators){
		relativeWeightsOfMutators[weight] = (cumulativeWeightsOfMutators[weight] / totalSumOfWeights)*100;
	}

	fs.appendFile("/dev/shm/mutatorResults.txt", iteration +","+JSON.stringify(relativeWeightsOfMutators)+"\n", function(err){

	});

}

function compareByScore(a,b) {
  if (a.score > b.score)
    return -1;
  if (a.score < b.score)
    return 1;
  return 0;
}

function geneticAlgorithm (pid, individualMutator, fuzzedFileName){


        var coverageData = null;
        try{
          var coverageDataFileName = prefs.getSancovOutputPrefix()[0] + "."+ pid + ".sancov";
          coverageData = fs.readFileSync(coverageDataFileName);
        }
        catch(e){
          logToConsole("Couldnt find coverage");
          //testCaseFunction(i+1);
          return;

        }

        var len=coverageData.length
        var indexOfCoverage=8;
        var resultCoverage=[]
        while(indexOfCoverage<len){
          var offset = protobuf.readUInt64(coverageData,indexOfCoverage,'little');
          resultCoverage.push(offset);
          indexOfCoverage = indexOfCoverage + 8;
        }
        fs.unlinkSync(coverageDataFileName);

        //Split the covered blocks into an array
        //var lines = stdout.toString().split('\n');

        //variable to keep track if we found a new block
        var foundNewBlock = 0;

        var results = resultsArray[0];

        //Go through each line and analyze if new blocks were covered
        resultCoverage.forEach(function(line) {
          if(line != ''){
            if(typeof results[line] === 'undefined'){
              //if new block found, mark foundNewBlock true
              results[line] = 1;
              foundNewBlock = 1;
            }
            else{
              //If old block, increase the amount of times block was covered
              results[line] = results[line]+1;
            }
          }
        });
        //If new block found add score to the individual and move fuzzed sample to sample directory
        if(foundNewBlock == 1){
          individualMutator.score = individualMutator.score + 10;
          try{
            var moveInterestingSample = "cp "+prefs.getFuzzedFileOutput()+fuzzedFileName+" "+prefs.getSamplePath()+"/"+fuzzedFileName;

            var rd = fs.createReadStream(prefs.getFuzzedFileOutput()+fuzzedFileName);
            var wr = fs.createWriteStream(prefs.getSamplePath()+"/"+fuzzedFileName);
            wr.on("close", function(ex) {
              newSample = {};
              newSample['name'] = fuzzedFileName;
              newSample['count'] = 0;
              samples.push(newSample);

              fs.unlink(prefs.getFuzzedFileOutput()+fuzzedFileName, function (err) {

              });

            });
            rd.pipe(wr);
          }
          catch(e){
            logToConsole("BBBBBBBBB");
            return;
          }


        }
        //if nothing exciting happened remove the sample
        else{
          try{
            fs.unlink(prefs.getFuzzedFileOutput()+fuzzedFileName, function (err) {

            });
          }
          catch(e){
            logToConsole("CCCCCCCCC");
            return;
          }
        }
        //Get random number for new generation probability
        var randomNewGenerationProbability = Math.random();

        //with five percent probability do  crossover
        if(randomNewGenerationProbability> 0.95){
          //Crossover

          //new crossover individuals
          var newIndividual1 = {};
          var newIndividual2 = {};

          //Get another individual to crossover with
          do{
            var anotherIndividualMutatorIndex = Math.floor(Math.random() * (weighedIndividualArray.length - 1 + 1) + 0);
          }while(anotherIndividualMutatorIndex == individualMutatorIndex);
          var anotherIndividualMutator = weighedIndividualArray[anotherIndividualMutatorIndex];

          //Make weight objects for new individuals
          var individualOneWeights = {};
          var individualTwoWeights = {};


          //var customWeights = individualMutator.weights;
          //iterate through every mutator and with 50-50 probability split them to new individuals
          for(var key in individualMutator.weights){
            var randomIndividualToCrossover = Math.random();
            if(randomIndividualToCrossover>0.5){
              individualOneWeights[key] = individualMutator.weights[key];
              individualTwoWeights[key] = anotherIndividualMutator.weights[key];
            }
            else{
              individualTwoWeights[key] = individualMutator.weights[key];
              individualOneWeights[key] = anotherIndividualMutator.weights[key];
            }



          }

          //create new fuzzer objects for the new individuals
          var firstNewIndividualFuzzer = new Surku({maxMutations:10,minMutations:1});
          var secondNewIndividualFuzzer = new Surku({maxMutations:10,minMutations:1});

          //Set up and push new individuals to the individual of arrays
          //TODO: Consider initial score. Maybe average of old individuals?
          newIndividual1['weights'] = individualOneWeights;
          newIndividual1['fuzzer'] = firstNewIndividualFuzzer;
          newIndividual1['score'] = 10;

          newIndividual2['weights'] = individualTwoWeights;
          newIndividual2['fuzzer'] = secondNewIndividualFuzzer;
          newIndividual2['score'] = 10;

          //console.log(newIndividual1);
          //console.log(newIndividual2);

          individualArray.push(newIndividual1);
          individualArray.push(newIndividual2);

        }
        //With five percent probability do mutation
        else if (randomNewGenerationProbability < 0.05){
          //Mutation
          //weights of mutators
          var customWeights = individualMutator.weights;
          //Prepare new mutated individual
          var mutatedIndividual = {}
          var mutatedIndividualWeights = {}

          //For each mutator decide if it will be mutated or not.
          for(var key in customWeights){
            var mutateOrNot = Math.random();
            //Mutate with 10% probability
            if(mutateOrNot < 0.10){
              //Mutate with amount of 1 to current weight
              var num = Math.floor(Math.random()*customWeights[key]) + 1;
              //Decide if we want to increase or decrease the weight mutator
              num *= Math.floor(Math.random()*2) == 1 ? 1 : -1;
              //Apply weight
              mutatedIndividualWeights[key] = customWeights[key] + num;
              //if weight is zero set it to 1
              if(mutatedIndividualWeights[key] < 1){
                mutatedIndividualWeights[key] = 1;
              }
            }
            else{
              //Apply weight
              mutatedIndividualWeights[key] = customWeights[key];
            }
          }

          //Create new individual and add it to the array
          var mutatedIndividualFuzzer = new Surku({maxMutations:10,minMutations:1});

          mutatedIndividual['weights'] = mutatedIndividualWeights;
          mutatedIndividual['fuzzer'] = mutatedIndividualFuzzer;
          //TODO: Consider if this is 10 or prev score
          mutatedIndividual['score'] = 10;
          individualArray.push(mutatedIndividual);
        }




        
}

process.on( 'SIGINT', function() {
  console.log( "\nGracefully shutting down from SIGINT (Ctrl-C)" );
  // some other closing procedures go here

  for(var j = 0; j < individualArray.length; j++){
    console.log("Fuzzer "+j+" score:" + individualArray[j].score);
  }

  process.exit( );
})
