# Automatic Mutator-balancing Genetic Algortihm (amga) #

This application aims to provide a fuzzer tool that automatically balances different mutators in a fuzzer to fit the filetype and system-under-test for maximizing the coverage. This balancing is done by utilizing a genetic algorithm that tries to maximize the covered code blocks.

For analysis amga uses SanitizerCoverage, and as a fuzzer node.js based general fuzzer Surku. However the plan is to extend amga so that any fuzzer that has command line arguments for setting mutator weights can be used.

## How do I? ##
1. Install node.js
2. Download Surku https://github.com/attekett/Surku
3. Get LLVM, version minimum 3.8. This is for the instrumentation
4. Compile your system that you want to test with Clang and enable SanitizerCoverage
5. Download this application and setup prefs.js file with your settings.
6. Run the tests: node index.js.


## TODO ##
- Effect of amount of fuzzing (min and max mutations per testcase)
- Sample quality (currently all samples are considered equally interesting)
- Optimizing things